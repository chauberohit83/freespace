const express = require('express');
const router = express.Router();

const {addUser, getUser} = require("../controllers/user");

router.get("/getUser", getUser);
router.post("/addUser", addUser);

module.exports = router;