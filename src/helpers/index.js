exports.successResponse = (req, res, data, success, code = 200) => res.send({
    code,
    data,
    success : success
}) 

exports.errorResponse = (req, res, errorMessage="Somthing Went Wrong", error, code = 500) => res.status(code).json({
    code,
    errorMessage,
    error,
    data: null,
    success: false
})