const { successResponse, errorResponse } = require("../helpers/index.js");
const fs = require("fs");
const user = require("../../user.json");

exports.addUser = async (req, res) => {
  try {
    const { name } = req.body;
    user.push(req.body);
    fs.writeFile("../../user.json", JSON.stringify(user), (err) => {
      if (err) {
        console.error(err);
      }
      return successResponse(
        req,
        res,
        "Data added succes fully",
        "Data Added Success"
      );
    });
  } catch (error) {
    return errorResponse(req, res, error.message);
  }
};

exports.getUser = async (req, res) => {
  try {
    return successResponse(req, res, user, "Data Added Success");
  } catch (error) {
    return errorResponse(req, res, error.message);
  }
};
