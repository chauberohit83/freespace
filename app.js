const http = require('http');
const server = http.createServer();
const SockJS = require('sockjs');
const sockjs = SockJS.createServer();
const cors = require('cors');
const corsOptions = {
    origin: 'http://signagesocket.s3-website.ap-south-1.amazonaws.com'
  };
  
server.on('request', cors());
sockjs.installHandlers(server, { prefix: '/socket' });

server.listen(3002, () => {
  console.log('Server listening on port 3002');
});

const connections = [];

sockjs.on('connection', (conn) => {
  console.log('New connection', conn.id);
  setInterval(() => {
    conn.write("Hello India");
  }, 5000);
});
// const express = require('express');
// const bodyParser = require('body-parser');
// const cors = require('cors');
// const morgan = require('morgan');
// require("dotenv").config();
// const SockJS = require('sockjs');
// const sockjs = SockJS.createServer();
// const app = express();

// const userRouter = require("./src/routes/user");


// app.use(cors({origin:[]}));
// app.use(morgan('dev'));
// app.use(bodyParser.json({limit:"5mb", type:"application/json"}));

// app.use("/dev", userRouter);

// sockjs.installHandlers(app, { prefix: '/socket' });

// const connections = [];

// sockjs.on('connection', (conn) => {
//   console.log('New connection', conn.id);
//   setInterval(() => {
//     conn.write("Hello India");
//   }, 5000);
// });

// const port = process.env.PORT || 5000;
// app.listen(port, () => {console.log(`Port started on ${port}`)});


